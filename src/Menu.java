import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * The Menu class is responsible for the view of the program.
 * @author Leonard Meerwood
 *
 */
public class Menu {

	//Scanner for input and Controller for control
	private Scanner sc;
	private Controller c;
	
	/**
	 * Initiates the default menu.
	 */
	public Menu() {
		sc = new Scanner(System.in);
		c = new Controller();
	}
	
	/**
	 * Intiates the default menu with a file preloaded.
	 * @param friends
	 * @param indexes
	 * 	File to be preloaded.
	 */
	public Menu(String friends, String indexes) {
		
		this();
		
		//Try load the file.
		try {
			c.loadFile(friends, indexes);
			System.out.println("FILE SUCCESSFULLY LOADED");
		
		//Handle the various exceptions thrown.
		} catch (FileNotFoundException e) {
			this.displayError("FILE PASSED VIA COMMAND LINE DOES NOT EXIST");
			
		} catch (IOException e) {
			this.displayError(e.getLocalizedMessage());
			
		} catch (NullPointerException e) {
			this.displayError("FILE PASSED VIA COMMAND LINE WAS INVALID. CHECK STRUCTURE.");
			
		} catch (Exception e) {
			this.displayError("FILE PASSED VIA COMMAND LINE INVALID.\nMESSAGE: " + e.getLocalizedMessage());
		}
	}
	
	/**
	 * Responsible for running the menu. Will display prompts and act according to the
	 * users reply.
	 */
	public void runMenu() {
		
		boolean running = true;
		
		//working loop.
		while (running) {
			System.out.println(mainMenuText);
			
			//The users input is wrapped inside of a try to allow us to handle
			//them putting in values besides numbers without the program crashing.
			try {
				int menuOption = sc.nextInt();
				
				//different actions depending on user input.
				switch (menuOption) {
				case 1:
					this.loadDialogue();
					break;
                case 2:
                    this.showFriendsOfFriends();
                    break;
                case 3:
                    this.showFriends();
                    break;
                case 4:
                    this.mutualFriends();
                    break;
				case 5:
					this.removeUser();
					break;
                case 6:
                	this.listUsers();
                    break;
				case 7:
					this.exitMessage();
					running = false;
					break;
				default:
					this.invalidInputMessage();
				}
			} catch (Exception e) {
				//This catch is what handles non numeric inputs.
				sc.nextLine();
				this.invalidInputMessage();
			}	
		}
	}
	
	/**
	 * Prompts the user for the file location that they wish to load. It then
	 * passes that to the {@link Controller} who tries to load it. If loading fails
	 * then an exception is thrown from the {@link Controller} and an appropriate
	 * error message is displayed.
	 */
	private void loadDialogue() {
		
		//Prompt for input
		System.out.println(loadIndex);
		//Retrieve input
		String indexes = sc.next();

		//Prompt for input
		System.out.println(loadRelations);
		//Retrieve input
		String friends = sc.next();
		
		//Try load the file.
		try {
			c.loadFile(friends, indexes);
		
		//Handle the various exceptions thrown.
		} catch (FileNotFoundException e) {
			this.displayError(e.getLocalizedMessage());
			
		} catch (IOException e) {
			this.displayError(e.getLocalizedMessage());
			
		} catch (NullPointerException e) {
			this.displayError("File was not in the correct format. Please check file and try again.");
		} catch (Exception e) {
			this.displayError(e.getLocalizedMessage() + " Please check file structure and try again.");
		}
		
	}

	/**
	 * Responsible for the interaction with user to get friends of a person.
	 */
	private void showFriends() {
	    System.out.println("Enter name of user: ");
	    String user = sc.next();
	    try {
	        String[] friends = c.friends(user);
	        System.out.println(user + " has " + friends.length + " friends.");
	        for (String f: friends) {
	            System.out.println(f);
            }
        } catch (Exception e) {
	        System.out.println("It appears there is no user with that name. Please try again.");
        }
    }

	/**
	 * Responsible for the interaction with user to get mutual friends between to people
	 */
	private void mutualFriends() {
		System.out.println("Enter name of first user: ");
		String userA = sc.next();
		System.out.println("Enter name of second user: ");
		String userB = sc.next();
		try {
			String[] mutualFriends = c.mutualFriends(userA, userB);
			System.out.println(userA + " and " + userB + " have " + mutualFriends.length + " mutual friend(s).");
			for (String f: mutualFriends) {
				System.out.println(f);
			}
		}catch (Exception e) {
			System.out.println("It appears one or more of those users don't exist. Please try again.");
		}
	}

	/**
	 * Responsible for the interaction with user to remove a person
	 */
    private void removeUser() {
	    System.out.println("Enter name of user to remove: ");
	    String user = sc.next();
	    try {
	        System.out.println("Are you sure you wish to delete " + user + "? (Y/n)");
	        String response = sc.next();
	        if (response.equalsIgnoreCase("y")) {
                c.removeUser(user);
                System.out.println("Successfully deleted user");
            } else {
	            System.out.println("Answer other than 'Y' received. Cancelling deletion");
            }

        } catch (Exception e) {
	        System.out.println("No such user to delete. Please try again.");
        }
    }

	/**
	 * Responsible for displaying all the users and their friend count.
	 */
	private void listUsers() {
		System.out.println("Friends | User");
		System.out.println("--------|-----");
		for (String f: c.neighboursAndLabels()) {
			String[] data = f.split(",");
			System.out.println(data[0].replaceFirst("^0+(?!$)", "") + "       | " + data[1]);
		}
	}

	/**
	 * Responsible for handling user interaction to display friends and friends of friends of a person
	 */
    private void showFriendsOfFriends() {
        System.out.println("Enter name of user: ");
        String user = sc.next();
        try {
            String[] friends = c.friendsOfFriends(user);
            System.out.println(user + " has " + friends.length + " friends and friends of friends.");
            for (String f: friends) {
                System.out.println(f);
            }
        } catch (Exception e) {
            System.out.println("It appears there is no user with that name. Please try again.");
        }
    }

	/**
	 * A simple function to display a formatted error message.
	 * @param error
	 * 	The error to be included in the message.
	 */
	private void displayError(String error) {
		String errorMessage = String.format(errorString, error);
		System.out.println(errorMessage);
	}

	/**
	 * Prints an exit message.
	 */
	private void exitMessage() {
		System.out.println(exitProgram);
	}
	
	/**
	 * Print a message stating invalid input.
	 */
	private void invalidInputMessage() {
		System.out.println(invalidInput);
	}
	
	
	//These are String constants used for longer messages.
	private final String mainMenuText = 
        "=========================================================\n"
        + " Press 1 to read information from file\n"
        + " Press 2 to find friends and friends of friends for a user\n"
        + " Press 3 to find friends of a user\n"
        + " Press 4 to find mutual friends of two users\n"
		+ " Press 5 to remove a user\n"
        + " Press 6 to show all users and the number of friends the have\n"
        + " Press 7 to exit the program\n"
        + "=========================================================";
	
	private final String loadIndex =
			"Please enter the file name of the user index: ";

	private final String loadRelations =
			"Please enter the file name of the friendships: ";
	
	private final String exitProgram = 
			"Thanks for using the program!\n";
	
	private final String invalidInput =
			"That is not a menu option. Please choose from option 1-7\n";
	
	private final String errorString = 
			"Error! %s\n";
}
