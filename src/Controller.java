import Graph.UndirectedGraph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * The Controller class provides an interface between {@link Menu} and
 * {@link Graph.UndirectedGraph}.
 * @author Leonard Meerwood
 *
 */
public class Controller {
	
	//An undirected graph
    private UndirectedGraph graph;
	
	//A flag to see if the list is loaded or not
	private boolean graphLoaded;
	
	/**
	 * Contrustor sets the graphLoaded flag to false
	 */
	public Controller() {
		this.graphLoaded = false;
	}
	
	/**
	 * Creates a graph with the size of the friends file. Then
     * adds links between the friends from the index file.
	 * @param friends
	 * @param indexes
	 *  The file to be loaded.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void loadFile(String friends, String indexes) throws FileNotFoundException, IOException, Exception {
		
		//If the filenames don't end in .txt, it is added.
		if (!friends.endsWith(".txt")) {
			friends = friends + ".txt";
		}
		if (!indexes.endsWith(".txt")) {
			indexes = indexes + ".txt";
		}
		
		//A try-with-resources block that tries to load the users.
		try (BufferedReader br = new BufferedReader(new FileReader(indexes)))  {
		    int size = Integer.parseInt(br.readLine());
		    this.graph = new UndirectedGraph(size);
		    String line;
		    for (int i = 0; i < size; i++) {
		        line = br.readLine();
		        String[] lineSplit = line.split(" ");
		        int index = Integer.parseInt(lineSplit[0]);
		        this.graph.setLabel(index, lineSplit[1]);
            }
		} catch (NullPointerException e) {
            throw new Exception("There appears to be a mismatch in the stated amount of users and the actual " +
                    "amount of users.");
        } catch (Exception e) {
		    throw new Exception("An error occured when loading the indexes.");
        }

        //A try-with-resources block that tries to load the relationships between users.
        try (BufferedReader br = new BufferedReader(new FileReader(friends)))  {
            int size = Integer.parseInt(br.readLine());

            for (int i = 0; i < size; i++) {
                String input = br.readLine();
                String[] inputs = input.split(" ");
                int source = Integer.parseInt(inputs[0]);
                int target = Integer.parseInt(inputs[1]);
                graph.addEdge(source, target);
            }
        } catch (NullPointerException e) {
		    throw new Exception("There appears to be a mismatch in the stated amount of relationships and the actual " +
                    "amount of relationships.");
        }
        catch (Exception e) {
            throw new Exception("An error occured when loading the friend relationships. " + e.getMessage());
        }
        this.graphLoaded = true;
	}

    /**
     * A function to find out a users friends
     * @Param user
     *  The name of the user to be checked
     */
    public String[] friends(String userName) throws IllegalStateException, NoSuchFieldException{
        this.checkFileLoaded();
        int userIndex = this.userIndex(userName);
        int[] neighbourIndex = graph.neighbors(userIndex);

        String[] output = new String[neighbourIndex.length];
        for (int i = 0; i < neighbourIndex.length; i++) {
            output[i] = (String) graph.getLabel(neighbourIndex[i]);
        }
        Arrays.sort(output);

        return output;
    }

    /**
     * The function to remove a user from the graph. According to the lecturer we should not delete the user, but
     * instead remove all their edges to other friends.
     * @param userName
     *  User to remove
     * @throws NoSuchFieldException
     *  If the user isn't found it will throw an exception
     */
    public void removeUser(String userName) throws NoSuchFieldException{
        this.checkFileLoaded();
        graph.removeLabel(this.userIndex(userName));

    }

    /**
     * A function to find friends and friends of friends.
     * @param userName
     *  The person to find relations of
     * @return
     *  A list of relations in a String array. It will NOT include the person whom the relations are about even though
     *  the example had that.
     * @throws NoSuchFieldException
     *  Thrown if user doesn't exist. This includes names not in the graph and names with zero edges (meaning they're
     *  essentially deleted.)
     *
     */
    public String[] friendsOfFriends(String userName) throws NoSuchFieldException{
        String output = "";
        String[] friends = this.friends(userName);
        for (String friend: friends) {
            String[] friendsOfFriend = this.friends(friend);
            if (!output.contains(friend)) {
                output += friend + ",";
            }
            for (String f: friendsOfFriend) {
                if (!f.equalsIgnoreCase(userName) && !output.contains(f))
                output += f + ",";
            }
        }
        output = output.substring(0, output.length() - 1);
        String[] outputArray = output.split(",");

        Arrays.sort(outputArray);
        return outputArray;
    }

    /**
     * Takes two users and returns the users that they share. User order doesn't matter.
     * @param userA
     *  First user
     * @param userB
     *  Second user
     * @return
     *  A String array holding all the mutual friends
     * @throws NoSuchFieldException
     *  Thrown if user doesn't exist. This includes names not in the graph and names with zero edges (meaning they're
     *  essentially deleted.)
     */
    public String[] mutualFriends(String userA, String userB) throws NoSuchFieldException{
        String[] friendsA = this.friends(userA);
        String[] friendsB = this.friends(userB);
        String output = "";
        for (String f: friendsA) {
            if (Arrays.stream(friendsB).anyMatch(f::equalsIgnoreCase)) {
                output += f + ",";
            }
        }
        output = output.substring(0,output.length() - 1);
        return output.split(",");
    }

    /**
     * Traverses the indexes of the graph and searches for a matching name. If found it returns the index.
     * @param userName
     *  User to find index of
     * @return
     *  An int containing the index of the user
     * @throws NoSuchFieldException
     *  Thrown if user doesn't exist. This includes names not in the graph and names with zero edges (meaning they're
     *  essentially deleted.)
     */
    private int userIndex(String userName) throws NoSuchFieldException {
        try {
            int sizeOfGraph = graph.size();
            String edgeName;
            for (int i = 0; i < sizeOfGraph; i++) {
                edgeName = (String) graph.getLabel(i);  //Safe cast. We only use strings for labels in this program
                if (edgeName.equalsIgnoreCase(userName)) {
                    return i;
                }
            }
            throw new NoSuchFieldException("User does not exist on the graph.");
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Will traverse the graph by the indexes and get all the users and store them in a {@Link UserTuple} array. It
     * then uses quicksort to order them based on friend count then name.
     * @return
     *  An array containing all users and friend counts in order
     */
    public String[] neighboursAndLabels() {
        int count = 0;
        for (int i = 0; i < graph.size(); i++) {
            if (graph.neighbors(i).length > 0) {
                count += 1;
            }
        }
        UserTuple[] results = new UserTuple[count];
        count = 0;
        for (int i = 0; i < graph.size(); i++) {
            if (graph.neighbors(i).length > 0) {
                results[count] = new UserTuple(graph.neighbors(i).length, (String) graph.getLabel(i));
                count++;
            }
        }
        quicksort(results);
        String[] output = new String[results.length];
        for(int i = 0; i < results.length; i++) {
            output[output.length - i - 1] = String.format("%d,%s",results[i].friends, results[i].name);
        }
        return output;
    }

    /**
     * Simply calls {@Link quicksort} with first = 0 and n = array length
     * @param data
     */
    public void quicksort(UserTuple[] data) {
        quicksort(data, 0, data.length);
    }

    /**
     * A method used to quick sort UserTuples. This was presented to us in Week 11
     * @param data
     *  Array holding the strings to be sorted
     * @param first
     *  The element to start the sort from
     * @param n
     *  How many elements from the first to sort
     */
    public void quicksort(UserTuple[ ] data, int first, int n)
    {
        int pivotIndex; // Array index for the pivot element
        int n1;         // Number of elements before the pivot element
        int n2;         // Number of elements after the pivot element

        if (n > 1)
        {
            // Partition the array, and set the pivot index.
            pivotIndex = partition(data, first, n);

            // Compute the sizes of the two pieces.
            n1 = pivotIndex - first;
            n2 = n - n1 - 1;

            // Recursive calls will now sort the two pieces.
            quicksort(data, first, n1);
            quicksort(data, pivotIndex + 1, n2);
        }
    }

    /**
     * A helper function
     * @param data
     * @param first
     * @param n
     * @return
     */
    private static int partition(UserTuple[ ] data, int first, int n)
    {

        UserTuple pivot = data[first];
        int tooBigIndex = first+1; //index of element after pivot
        int tooSmallIndex = first + n -1; // index of last element

        while(tooBigIndex <= tooSmallIndex){
            while(tooBigIndex < first+n-1 && data[tooBigIndex].compareTo(pivot) == -1 ){
                tooBigIndex++;
            }

            while (data[tooSmallIndex].compareTo(pivot) == 1){
                tooSmallIndex--;
            }

            if(tooBigIndex <= tooSmallIndex){
                swap(data, tooBigIndex, tooSmallIndex);

                tooBigIndex++;
            }
        }

        data[first]=data[tooSmallIndex];
        data[tooSmallIndex]= pivot;

        return tooSmallIndex;
    }

    /**
     * Simply swaps two elements in an array.
     * @param data
     *  Array to swap.
     * @param tooBigIndex
     *  index of first element
     * @param tooSmallIndex
     *  index of second element
     */
    private static void swap(UserTuple[ ] data, int tooBigIndex, int tooSmallIndex)
    {
        UserTuple temp = data[tooBigIndex]; // store the value before overwriting

        data[tooBigIndex] = data[tooSmallIndex];
        data[tooSmallIndex] = temp;
    }




	/**
	 * A quick check to see if the listLoaded flag is set. If not, throws 
	 * an exception.
	 * @throws IllegalStateException
	 */
	private void checkFileLoaded() throws IllegalStateException {
		if (!graphLoaded) {
			throw new IllegalStateException("No file has been loaded.");
		}
	}

    /**
     * A small helper class to assist in comparing users and their friend counts.
     */
	private class UserTuple implements Comparable<UserTuple> {
	    public final int friends;
	    public final String name;

        /**
         * Constructor
         * @param friends
         *  Count of friends
         * @param name
         *  Name of user
         */
	    public UserTuple(Integer friends, String name) {
	        this.friends = friends;
	        this.name = name;
        }

        /**
         * Implementation of the compareTo function. The rules are:
         * - User with higher friends is sorted ahead of the one with lower friends
         * - If both users have the same friend count then they are sorted alphabetically.
         * @param other
         *  The other user to compare to.
         * @return
         *  An int between -1 and 1
         */
        @Override
        public int compareTo(UserTuple other) {
	        if(friends > other.friends) {
	            return 1;
            } else if (friends < other.friends) {
	            return -1;
            } else {
	            int nameCompare = name.compareTo(other.name);
	            if(nameCompare > 0) {
	                return -1;
                } else if (nameCompare < 0) {
	                return 1;
                } else {
	                return nameCompare;
                }
            }
        }

    }
}
