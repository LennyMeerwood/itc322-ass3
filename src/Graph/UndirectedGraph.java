package Graph;

public class UndirectedGraph extends Graph{

    /**
     * Initialize an UndirectedGraph with n vertices,
     * no edges, and null labels.
     * @param n
     *   the number of vertices for this Graph
     * Precondition:
     *   n is nonnegative.
     * Postcondition:
     *   This Graph has n vertices, numbered
     *   0 to n-1. It has no edges and all
     *   vertex labels are null.
     * @exception OutOfMemoryError
     *   Indicates insufficient memory for the specified number of nodes.
     * @exception NegativeArraySizeException
     *   Indicates that n is negative.
     */
    public UndirectedGraph(int n) {
        super(n);
    }

    /**
     * Add a new edge to this Graph. Because it is an undirected graph the edge
     * will need to be added in both directions.
     * @param source
     *   the vertex number of the source of the new edge
     * @param target
     *   the vertex number of the target of the new edge
     * Precondition:
     *   Both source and target are nonnegative and
     *   less than size().
     * Postcondition:
     *   This Graph has all the edges that it originally had plus
     *   another edge from the specified source to the specified
     *   target. (If the edge was already present, then this
     *   Graph is unchanged.)
     * @exception ArrayIndexOutOfBoundsException
     *   Indicates that the source or target was not a
     *   valid vertex number.
     **/
    @Override
    public void addEdge(int source, int target)
    {
        super.addEdge(source, target);
        super.addEdge(target, source);
    }

    /**
     * Remove an edge from this Graph. Because it's an undirected graph the
     * edges must be removed from both directions.
     * @param source
     *   the vertex number of the source of the removed edge
     * @param target
     *   the vertex number of the target of the removed edge
     * Precondition:
     *   Both source and target are nonnegative and
     *   less than size().
     * Postcondition:
     *   This Graph has all the edges that it originally had minus
     *   the edge from the specified source to the specified
     *   target. (If the edge was not present, then this
     *   Graph is unchanged.)
     * @exception ArrayIndexOutOfBoundsException
     *   Indicates that the source or target was not a
     *   valid vertex number.
     **/
    @Override
    public void removeEdge(int source, int target)
    {
        super.removeEdge(source, target);
        super.removeEdge(target, source);
    }

    /**
     * Accessor method to get the label of a vertex of this Graph.
     * @param vertex
     *   a vertex number
     * Precondition:
     *   vertex is nonnegative and
     *   less than size().
     * @return
     *   the label of the specified vertex in this Graph
     * @exception ArrayIndexOutOfBoundsException
     *   Indicates that the vertex was not a
     *   valid vertex number.
     **/
    public Object getLabel(int vertex)
    {
        if (super.neighbors(vertex).length > 0) {
            return super.getLabel(vertex);
        } else {
            return (new NoSuchFieldException("user doesn't exist"));
        }
    }

    public void removeLabel(int vertix) {
        int[] neighbours = this.neighbors(vertix);
        for (int n: neighbours) {
            this.removeEdge(vertix, n);
        }
    }
}
