/**
 * This is the entry point for my assignment 1 submission.
 * @author Leonard Meerwood
 *
 */
import Graph.*;

public class main {
	public static void main(String[] args) {
		//Make a new menu (view) instance and run it.
		Menu menu;
		if (args.length >= 2) {
			menu = new Menu(args[0], args[1]);
		} else {
			menu = new Menu();
		}
		menu.runMenu();

	}
}
